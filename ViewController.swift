//
//  ViewController.swift
//  navigationbar
//
//  Created by Nazar Starantsov on 05.03.2021.
//

import UIKit

final class ViewController: BaseViewController {
    let scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "пульс"
        
        addSubviews()
    }

    private func addSubviews() {
        view.addSubview(scrollView)
        scrollView.frame = view.frame
        
        scrollView.backgroundColor = .cyan
        scrollView.showsVerticalScrollIndicator = true
        
        scrollView.contentSize = CGSize(width: 300, height: 3000)
    }

}

