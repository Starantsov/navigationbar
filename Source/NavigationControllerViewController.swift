//
//  NavigationControllerViewController.swift
//  navigationbar
//
//  Created by Nazar Starantsov on 05.03.2021.
//

import UIKit

class NavigationController: UINavigationController {
    private var appearance = UINavigationBarAppearance()
    private var visualEffectView: VisualEffectView?
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        //view.backgroundColor = .systemBackground
        //hidesBarsOnSwipe = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.standardAppearance = appearance
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        restoreNavigationBar()
    }
    
    // MARK: - Helpers
    
    private func setupNavigationBar() {
        // Insert blur view if need
        // insertBlurView()
    }

    
    /// Insets visual effect view with custom blur into navigation bar
    private func insertBlurView() {
        let navBar = navigationBar
        
        // Creating own blur view and inserting it inside navigation bar
        let visualEffectView = VisualEffectView.makeDefaultEffectView(colorTint: .systemBackground)
        visualEffectView.isUserInteractionEnabled = false
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.layer.zPosition = -1

        navBar.addSubview(visualEffectView)
        
        NSLayoutConstraint.activate([
            visualEffectView.leadingAnchor.constraint(equalTo: navBar.leadingAnchor),
            visualEffectView.trailingAnchor.constraint(equalTo: navBar.trailingAnchor),
            visualEffectView.bottomAnchor.constraint(equalTo: navBar.bottomAnchor),
            visualEffectView.heightAnchor.constraint(equalTo: navBar.heightAnchor),
        ])
        
        self.visualEffectView = visualEffectView
    }
    
    /// Removing blurred view from navigationBar to prevent stacking
    /// with the same one added when view will reappear
    /// Removing lineView for the same reason
    private func restoreNavigationBar() {
        visualEffectView?.removeFromSuperview()
        visualEffectView = nil
    }
}

