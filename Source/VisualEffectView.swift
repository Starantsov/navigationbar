//
//  VisualEffectView.swift
//  navigationbar
//
//  Created by Nazar Starantsov on 05.03.2021.
//

import UIKit

public class VisualEffectView: UIVisualEffectView {
    private let blurEffect = (NSClassFromString("_UICustomBlurEffect") as! UIBlurEffect.Type).init()

    public var colorTint: UIColor? {
        get { _value(forKey: "colorTint") as? UIColor }
        set { _setValue(newValue, forKey: "colorTint") }
    }

    public var tintColorAlpha: CGFloat {
        get { _value(forKey: "colorTintAlpha") as! CGFloat }
        set { _setValue(newValue, forKey: "colorTintAlpha") }
    }

    public var blurRadius: CGFloat {
        get { _value(forKey: "blurRadius") as! CGFloat }
        set { _setValue(newValue, forKey: "blurRadius") }
    }

    private func _value(forKey key: String) -> Any? {
        blurEffect.value(forKeyPath: key)
    }

    private func _setValue(_ value: Any?, forKey key: String) {
        blurEffect.setValue(value, forKeyPath: key)
        effect = blurEffect
    }

    public static func makeDefaultEffectView(
        blurRadius: CGFloat = 30,
        colorTint: UIColor = .systemGroupedBackground
    ) -> VisualEffectView {
        let visualEffectView = VisualEffectView()
        
        if #available(iOS 14, *) {
            visualEffectView.effect = UIBlurEffect(style: .regular)
        } else {
            visualEffectView.colorTint = colorTint
            visualEffectView.tintColorAlpha = 0.5
            visualEffectView.blurRadius = blurRadius
        }
        
        return visualEffectView
    }
}
