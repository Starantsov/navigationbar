//
//  NavigationBarView.swift
//  navigationbar
//
//  Created by Nazar Starantsov on 09.03.2021.
//

import SwiftUI
    
struct NavigationBarView: View {
    var body: some View {
        HStack(spacing: 20) {
            Button(action: { print("burger pressed")}) {
                Image(systemName: "line.horizontal.3")
                    .foregroundColor(.black)
                    .font(.system(size: 17, weight: .semibold))
            }
            
            Spacer()
            
            Button(action: { print("search pressed")}) {
                Image(systemName: "magnifyingglass")
                    .foregroundColor(.black)
            }
            
            Button(action: { print("search pressed")}) {
                Image(systemName: "bell")
                    .foregroundColor(.black)
            }
            
            Button(action: { print("avatar pressed")}) {
                Image(systemName: "person.circle")
                    .foregroundColor(.black)
                    .font(.system(size: 19))
            }
        }
        .padding()
        //.background(Color(.systemGroupedBackground))
    }
}

struct NavigationBarView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBarView()
    }
}
