//
//  BaseViewController.swift
//  navigationbar
//
//  Created by Nazar Starantsov on 05.03.2021.
//

import SwiftUI

class BaseViewController: UIViewController {
    let statusBarView: UIView = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStatusBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        guard let navController = navigationController else { return }
        
        title = ""

        // Remove current background and devider from navbar
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.shadowColor = .clear

        navController.navigationBar.standardAppearance = navBarAppearance
        navController.navigationBar.layoutSubviews()
        navController.hidesBarsOnSwipe = true
        
        addNavigationView()
    }
    
    /// So status bar has background when navigation bar swipes up
    private func setupStatusBar() {
        guard let navController = navigationController else { return }
        
        navController.view.addSubview(statusBarView)
        
        statusBarView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        statusBarView.layer.zPosition = 1
    }
    
    private func addNavigationView() {
        guard let navController = navigationController else { return }
        
        let navigationBar = NavigationBarView()
        
        guard let navigationView = UIHostingController(rootView: navigationBar).view else {
            return
        }
        navController.navigationBar.addSubview(navigationView)
        navigationView.frame.size = navController.navigationBar.frame.size
        
        navigationView.backgroundColor = .clear
        
        navController.navigationBar.bringSubviewToFront(statusBarView)
    }
}
